let data = {
    'mobie-dick': {
        title: 'Mobie dick',
        author: ''
    }, 
    'karamazov-bratia': {
        title: 'Karamazov bratia',
        author: ''
    }, 
    'tychon': {
        title: 'Tychon',
        author: ''
    }
};

export async function getAllBooks() {
    return await Promise.resolve(data);
}

export async function getOneBook(id) {
    if (id in data) {
        return await Promise.resolve(data[id]);
    } else {
        throw new Error(`Not valid book ${id}`);
    }
}

export async function createBook(book) {
    const id = book.id;
    if (!(id in data)) {
        data[id] = book;
        return await Promise.resolve(book);
    } else {
        throw new Error(`Exists already ${id}`);
    }
}
