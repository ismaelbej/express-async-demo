import express from 'express';
import * as Book from '../controllers/book';

const router = express.Router();

router.get('/books', async (req, res) => {
    try {
        const books = await Book.getAllBooks();
        res.json(books);
    } catch (e) {
        res.json({ error: e.message });
    }
});

router.get('/book/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const book = await Book.getOneBook(id);
        res.json(book);
    } catch(e) {
        res.json({ error: e.message });
    }
});

router.post('/book', async (req, res) => {
    try {
        const book = await Book.createBook(req.body);
        res.json(book);
    } catch (e) {
        res.json({ error: e.message });
    }
});

export default router;
