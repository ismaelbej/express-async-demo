import express from 'express';
import book from './routes/book';

const app = express();

app.use('/api/v1', book);

const port = process.env.PORT || 3000;

app.listen(port, (err) => {
    if (err) {
        console.error(err);
        return;
    }
    if (__DEV__) {
        console.log('****In development****');
    }
    console.log('Listening on port ' + port);
});
